# README #

Carpooling application coded during hackathon. 

### Used apis ###
* google plus api for authentication and user registration
* google maps api
* google geolocation api

### How do I get set up? ###

* Build with maven using `mvn install`
* Add to host file:
```
127.0.0.1       carpooling1116.com
```
* Start from the main class com.hackathon.DemoApplication
* In the browser navigate to `http://carpooling1116.com:8080/`