package com.hackathon;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;


public class Route {
    private final String routeId;
    private final String userName;
    private final Double x;
    private final Double y;
    private final String address;
    private final Integer noOfPlaces;
    private final LocalDateTime date;
    private final DirectionMode directionMode;
    private final Set<Passenger> passengers = new CopyOnWriteArraySet<>();

    public Route(String userName, Double x, Double y, String address, Integer noOfPlaces, LocalDateTime date, DirectionMode directionMode) {
        this.address = address;
        this.date = date;
        this.directionMode = directionMode;
        this.routeId = UUID.randomUUID().toString();
        this.userName = userName;
        this.x = x;
        this.y = y;
        this.noOfPlaces = noOfPlaces;
    }

    public String getUserName() {
        return userName;
    }

    public Integer getNoOfPlaces() {
        return noOfPlaces;
    }

    public String getRouteId() {
        return routeId;
    }

    public Double getX() {
        return x;
    }

    public Double getY() {
        return y;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public DirectionMode getDirectionMode() {
        return directionMode;
    }

    public Set<Passenger> getPassengers() {
        return passengers;
    }

    public String getAddress() {
        return address;
    }

    public int getFreePlaces(){
        return noOfPlaces - getPassengers().size();
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
