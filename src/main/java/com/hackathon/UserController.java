package com.hackathon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping("/user")
    public User getUser(Principal principal) {
        return userService.createFromPrincipal(principal);
    }
}
