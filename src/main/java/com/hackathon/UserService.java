package com.hackathon;

import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Map;

@Service
public class UserService {
    public User createFromPrincipal(Principal principal) {
        OAuth2Authentication authentication = (OAuth2Authentication) principal;
        Map userDetails = (Map) authentication.getUserAuthentication().getDetails();
        return new User(
                (String) userDetails.get("id"),
                (String) userDetails.get("displayName"),
                (String) userDetails.get("url"),
                (String) ((Map) userDetails.get("image")).get("url")
        );
    }
}
