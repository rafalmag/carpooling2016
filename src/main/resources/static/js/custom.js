//var map;
var userInfo;
function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 51.106922, lng: 17.037552},
        zoom: 14,
        disableDoubleClickZoom: true
    });

    google.maps.event.addListener(map, 'dblclick', function(event) {
       addRoute(event.latLng);
    });

    var geocoder = new google.maps.Geocoder;
    function addRoute(location) {
        var address = location.lat() + " " + location.lng();
        geocoder.geocode({'location': location}, function(results, status) {
              if (status === 'OK') {
                    if (results[0]) {
                        address = results[0].formatted_address;
                    } else {
                        console.log('No result[0], status: ' + status);
                    }
              } else {
                    console.log('Geocoder failed due to: ' + status);
              }
              addRouteWithAddress(location, address);
        });
    }

    function addRouteWithAddress(location, address) {
        var now = new Date();
        $("#addRouteModal #Lat").val(location.lat());
        $("#addRouteModal #Lng").val(location.lng());
        $("#addRouteModal #Date").val(now.toISOString().slice(0,10));
        // isoString returns time in GMT+0, so I changed to localTime
        // fixme potential issue around midnight (wrong date above) and potential issues with 12h locales...
        $("#addRouteModal #Time").val(now.toLocaleTimeString().slice(0,5));
        $("#addRouteModal #Address").val(address);
        $("#addRouteModal #NoOfPlaces").val(3);
        $("#addRouteModal #UserPic").attr("src", userInfo.imageUrl.replace(/sz=\d+$/, "sz=116"));
        $("#addRouteModal #Driver").html(userInfo.displayName);
        $("#addRouteModal").modal('show');
    }

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };


            map.setCenter(pos);
        }, function() {
        });
    } else {
        // Browser doesn't support Geolocation
    }

    var routeData= new Object();

    var markers = new Object();

    $.get("/getRoutes?directionMode=TO_WORK&startDate=A&endDate=A", function(data, status){
        $.each( data, function(i, value) {
            var routeId = value.routeId;
            routeData[value.routeId] = value;
            var latLng = new google.maps.LatLng(value.x,value.y);
            var image = "/images/marker1.png";
            var marker = new google.maps.Marker({
                position: latLng,
                map: map,
                icon: image
            });
            marker.addListener('click', function() {
                $("#addToRouteModal #Id").html(routeData[routeId].routeId);
                $("#addToRouteModal #RouteId").html(routeData[routeId].routeId);
                $("#addToRouteModal #Date").html(routeData[routeId].date.split("T")[0]);
                $("#addToRouteModal #Time").html(routeData[routeId].date.split("T")[1].slice(0,5));
                $("#addToRouteModal #Address").html(routeData[routeId].address);
                $("#addToRouteModal #FreePlaces").html(routeData[routeId].freePlaces);
                $("#addToRouteModal").modal('show');
            });
            markers[routeId] = marker;
        });
    });
    var latLng = new google.maps.LatLng(51.106922, 17.037552);
    var marker = new google.maps.Marker({
        position: latLng,
        map: map,
        icon: "/images/markerRed.png"
    });

    $("#addToRouteModal #ReservePlace").click(function() {
        var routeId = $("#addToRouteModal #RouteId").html();
        markers[routeId].setIcon("/images/marker2.png");
        $.ajax({
            url:"/addPassenger?routeId="+routeId+"&userName="+userInfo.id,
            success:function(value) {
                routeData[routeId]=value;
            }
        });
    });
    $("#addRouteModal #AddRoute").click(function() {
        var lat = $("#addRouteModal #Lat").val();
        var lng = $("#addRouteModal #Lng").val();
        var address = $("#addRouteModal #Address").val();
        var noOfPlaces = $("#addRouteModal #NoOfPlaces").val();
        //TODO hardcoding
        var directionMode = "TO_WORK";
        // TODO validate date (but it already too late, as we will dismiss the modal)
        var date = $("#addRouteModal #Date").val()+"T"+$("#addRouteModal #Time").val()+":00.000";
        var image = "/images/marker3.png";
        $.ajax({
            url:"/addRoute?userName="+userInfo.id
                        +"&x="+lat+"&y="+lng
                        +"&address="+address
                        +"&noOfPlaces="+noOfPlaces
                        +"&date="+date
                        +"&directionMode="+directionMode,
            success:function(value) {
                routeData[value.routeId]=value;
                var marker = new google.maps.Marker({
                    position: {
                                lat: parseFloat(lat),
                                lng: parseFloat(lng)
                              },
                    map: map,
                    icon: image
                });
                marker.addListener('click', function() {
                    $("#editRouteModal").modal('show');
                });
            }
        });
    });
}

function getUserInfoAndInitLeftPanel() {
    $.ajax({

        url:"/user",

        success: function(u) {
            userInfo = u;
            $("#leftPanel #userpic").attr("src", userInfo.imageUrl.replace(/sz=\d+$/, "sz=96"));
            $("#leftPanel #Greetings").html("Hi " + userInfo.displayName + "!");
        }

    });
    $("#leftPanel #startTime").val(new Date().toLocaleTimeString());
}
