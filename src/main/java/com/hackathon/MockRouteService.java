package com.hackathon;


import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Service("mockRouteService")
public class MockRouteService extends RouteServiceImpl {

    @PostConstruct
    public void init() {
        // to routes
        Route driverBi1ToRoute = addRoute("driverBi1", 51.109151, 17.018084, 3, LocalDateTime.now().plus(10, ChronoUnit.MINUTES), DirectionMode.TO_WORK, "Legnicka 1");
        addPassenger(driverBi1ToRoute.getRouteId(), "passenger1");
        addPassenger(driverBi1ToRoute.getRouteId(), "passenger2");
        Route driverTo1RouteTo = addRoute("driverTo1", 51.114095, 17.019790, 2, LocalDateTime.now().plus(15, ChronoUnit.MINUTES), DirectionMode.TO_WORK, "Strzegomska 32");
        addPassenger(driverTo1RouteTo.getRouteId(), "passenger3");
        addRoute("driverBi2", 51.117054, 17.031014, 3, LocalDateTime.now().plus(20, ChronoUnit.MINUTES), DirectionMode.TO_WORK, "Pilsudskiego 22");

        // from routes
        Route driverBi1FromRoute = addRoute("driverBi1", 51.109151, 17.018084, 3, LocalDateTime.now().plus(5, ChronoUnit.HOURS), DirectionMode.FROM_WORK, "Legnicka 1");
        addPassenger(driverBi1FromRoute.getRouteId(), "passenger1");

        addRoute("driverBi2", 51.117054, 17.031014, 4, LocalDateTime.now().plus(6, ChronoUnit.HOURS), DirectionMode.FROM_WORK, "Strzegomska 32");
    }
}
