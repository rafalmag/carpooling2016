package com.hackathon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.Collection;

@RestController
public class RouteController {

    private static final Logger LOG = LoggerFactory.getLogger(RouteController.class);

    @Autowired
    @Qualifier("mockRouteService")
    RouteService routeService;

    @Autowired
    UserService userService;

    @RequestMapping("/addRoute")
    public Route addRoute(@RequestParam(value = "userName") String userName,
                          @RequestParam(value = "x") Double x,
                          @RequestParam(value = "y") Double y,
                          @RequestParam(value = "address") String address,
                          @RequestParam(value = "noOfPlaces") Integer noOfPlaces,
                          @RequestParam(value = "date") String date,
                          @RequestParam(value = "directionMode") DirectionMode directionMode,
                          Principal principal
    ) {
        if (!userService.createFromPrincipal(principal).getId().equals(userName)) {
            LOG.warn("principal user id does not equal reqest userName {}", userName);
        }
        return routeService.addRoute(userName, x, y, noOfPlaces, parseDate(date), directionMode, address);
    }

    private LocalDateTime parseDate(@RequestParam(value = "date") String dateString) {
        if (dateString == null) {
            LOG.warn("date is null");
            return null;
        }
        try {
            return LocalDateTime.parse(dateString);
        } catch (DateTimeParseException e) {
            LOG.warn("Could not parse date {}", dateString);
            return null;
        }
    }

    @RequestMapping("/getRoutes")
    public Collection<Route> getRoutes(@RequestParam(value = "directionMode") DirectionMode directionMode,
                                       @RequestParam(value = "startDate") String startDate,
                                       @RequestParam(value = "endDate") String endDate) {
        return routeService.getRoutes(directionMode, parseDate(startDate), parseDate(endDate));
    }

    @RequestMapping("/addPassenger")
    public Route addPassenger(@RequestParam(value = "routeId") String routeId,
                              @RequestParam(value = "userName") String userName) {
        return routeService.addPassenger(routeId, userName);
    }

    @RequestMapping("/cancelRoute")
    public void cancelRoute(@RequestParam(value = "routeId") String routeId) {
        routeService.cancelRoute(routeId);
    }

    @RequestMapping("/removePassenger")
    public Route removePassenger(@RequestParam(value = "routeId") String routeId,
                                 @RequestParam(value = "userName") String userName) {
        return routeService.removePassenger(routeId, userName);
    }
//
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("/js/**")
//                .addResourceLocations("classpath:/static/js/");
//    }

}
