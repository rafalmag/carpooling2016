package com.hackathon;

public class User {
    private String id;
    private String displayName;
    private String url;
    private String imageUrl;

    public User(String id, String displayName, String url, String imageUrl) {
        this.id = id;
        this.displayName = displayName;
        this.url = url;
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getUrl() {
        return url;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
