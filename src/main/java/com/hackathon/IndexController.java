package com.hackathon;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
public class IndexController {

    @RequestMapping("/")
    String index(Principal currentUser) {
        return "index";
    }
}
