package com.hackathon;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class RouteServiceTest {

    RouteService routeService;

    @Before
    public void setUp() {
        routeService = new RouteServiceImpl();
    }

    @Test
    public void shouldAddRoute() {
        // given
        String driver = "driver";
        LocalDateTime date = LocalDateTime.now();
        DirectionMode direction = DirectionMode.TO_WORK;
        double x = 1.0;
        double y = 2.0;
        int noOfPlaces = 3;

        // when
        Route route = routeService.addRoute(driver, x, y, noOfPlaces, date, direction, "address");
        // then
        assertThat(route.getRouteId()).isNotEmpty();
        assertThat(route.getPassengers()).isEmpty();
        assertThat(route.getDate()).isEqualTo(date);
        assertThat(route.getNoOfPlaces()).isEqualTo(noOfPlaces);
        assertThat(route.getX()).isEqualTo(x);
        assertThat(route.getY()).isEqualTo(y);
        assertThat(route.getDirectionMode()).isEqualTo(direction);
        assertThat(route.getFreePlaces()).isEqualTo(noOfPlaces);
    }

    @Test
    public void shouldGetRoutes() {
        // given
        String driver = "driver";
        LocalDateTime date = LocalDateTime.now();
        DirectionMode direction = DirectionMode.TO_WORK;
        LocalDateTime dateMinus1h = date.minus(1, ChronoUnit.HOURS);
        LocalDateTime datePlus1h = date.plus(1, ChronoUnit.HOURS);
        Route route = routeService.addRoute(driver, 1.0, 2.0, 3, date, direction, "address");

        // when + then
        assertThat(routeService.getRoutes(direction, null, dateMinus1h)).describedAs("start (null) < endDate < date ").isEmpty();
        assertThat(routeService.getRoutes(direction, null, date)).describedAs("start(null) < date = endDate").containsOnly(route);
        assertThat(routeService.getRoutes(direction, null, datePlus1h)).describedAs("start(null) < date < endDate").containsOnly(route);
        assertThat(routeService.getRoutes(direction, null, null)).describedAs("start end are null = no restriction").containsOnly(route);
        assertThat(routeService.getRoutes(direction, LocalDateTime.MIN, dateMinus1h)).describedAs("startDate(MIN) < endDate < date").isEmpty();
        assertThat(routeService.getRoutes(direction, dateMinus1h, date)).describedAs("startDate < date = endDate").containsOnly(route);
        assertThat(routeService.getRoutes(direction, dateMinus1h, datePlus1h)).describedAs("startDate < date < endDate").containsOnly(route);
        assertThat(routeService.getRoutes(direction, dateMinus1h, null)).describedAs("startDate < date < end(null)").containsOnly(route);
        assertThat(routeService.getRoutes(direction, date, datePlus1h)).describedAs("startDate = date < endDate").containsOnly(route);
        assertThat(routeService.getRoutes(direction, datePlus1h, LocalDateTime.MAX)).describedAs("date  < startDate < endDate").isEmpty();
        assertThat(routeService.getRoutes(direction, datePlus1h, null)).describedAs("date  < startDate < endDate(null)").isEmpty();
        assertThat(routeService.getRoutes(DirectionMode.FROM_WORK, dateMinus1h, datePlus1h)).isEmpty();
    }

    @Test
    public void shouldAddPassengerToRoute() {
        // given
        String driver = "driver";
        LocalDateTime date = LocalDateTime.now();
        DirectionMode direction = DirectionMode.TO_WORK;
        int noOfPlaces = 3;
        Route route = routeService.addRoute(driver, 1.0, 2.0, noOfPlaces, date, direction, "address");
        // when
        Route updatedRoute = routeService.addPassenger(route.getRouteId(), "passenger");
        // then
        assertThat(updatedRoute.getPassengers()).hasSize(1);
        Passenger passenger = updatedRoute.getPassengers().iterator().next();
        assertThat(passenger.getPassengerName()).isEqualTo("passenger");
        assertThat(updatedRoute.getFreePlaces()).isEqualTo(2);
    }

    @Test
    public void shouldCancelRoute() {
        // given
        String driver = "driver";
        LocalDateTime date = LocalDateTime.now();
        DirectionMode direction = DirectionMode.TO_WORK;
        Route route = routeService.addRoute(driver, 1.0, 2.0, 3, date, direction, "address");
        // when
        routeService.cancelRoute(route.getRouteId());
        // then
        assertThat(routeService.getRoutes(DirectionMode.FROM_WORK, null, null)).isEmpty();
        assertThat(routeService.getRoutes(DirectionMode.TO_WORK, null, null)).isEmpty();
    }

    @Test
    public void shouldRemovePassenger() {
        // given
        String driver = "driver";
        LocalDateTime date = LocalDateTime.now();
        DirectionMode direction = DirectionMode.TO_WORK;
        int noOfPlaces = 3;
        Route route = routeService.addRoute(driver, 1.0, 2.0, noOfPlaces, date, direction, "address");
        Route updatedRoute = routeService.addPassenger(route.getRouteId(), "passenger");
        // when
        routeService.removePassenger(route.getRouteId(), "passenger");
        // then
        assertThat(updatedRoute.getPassengers()).hasSize(0);
        assertThat(updatedRoute.getFreePlaces()).isEqualTo(noOfPlaces);
    }

    @Test(expected = CarPoolingException.class)
    public void shouldNotPassengerToNotFoundRoute() {
        // given
        // when
        routeService.addPassenger("noSuchRoute", "passenger");
    }

    @Test(expected = CarPoolingException.class)
    public void shouldNotAddDriverAsAPassengerToSameRoute() {
        // given
        String driver = "driver";
        LocalDateTime date = LocalDateTime.now();
        DirectionMode direction = DirectionMode.TO_WORK;
        int noOfPlaces = 3;
        Route route = routeService.addRoute(driver, 1.0, 2.0, noOfPlaces, date, direction, "address");
        // when
        routeService.addPassenger(route.getRouteId(), driver);
    }

}
