package com.hackathon;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
//@WebMvcTest(RouteController.class)
@RunWith(SpringRunner.class)
public class RouteControllerIntegrationTest {

    @Autowired
    private RouteController routeController;


    @Test
    public void shouldWork() throws Exception {
        // given
        Principal principal = Mockito.mock(Principal.class);
        UserService mockUserService = Mockito.mock(UserService.class);
        User user = new User("userId", "displayName", "url", "imageUrl");
        Mockito.when(mockUserService.createFromPrincipal(principal)).thenReturn(user);
        routeController.userService = mockUserService;
        // when
        routeController.addRoute("userId", 1.0, 2.0, "address", 3, "date", DirectionMode.TO_WORK, principal);
        // then
        assertThat(routeController.getRoutes(DirectionMode.TO_WORK, "startDate", "endDate")).hasSize(3+1);
        assertThat(routeController.getRoutes(DirectionMode.FROM_WORK, "startDate", "endDate")).hasSize(2);
    }
}