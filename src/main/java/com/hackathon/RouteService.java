package com.hackathon;

import java.time.LocalDateTime;
import java.util.Collection;


public interface RouteService {

    Route addRoute(String userName, Double x, Double y, Integer noOfPlaces, LocalDateTime date, DirectionMode directionMode, String address);

    Collection<Route> getRoutes(DirectionMode directionMode, LocalDateTime startDate, LocalDateTime endDate);

    Route addPassenger(String routeId, String userName);

    void cancelRoute(String routeId);

    Route removePassenger(String routeId, String userName);

}
