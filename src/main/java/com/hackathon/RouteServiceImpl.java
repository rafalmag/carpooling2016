package com.hackathon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
public class RouteServiceImpl implements RouteService {

    private static final Logger LOG = LoggerFactory.getLogger(RouteServiceImpl.class);

    private Map<String, Route> routesDatabase = new ConcurrentHashMap<>();

    public Route addRoute(String userName, Double x, Double y, Integer noOfPlaces, LocalDateTime date, DirectionMode directionMode, String address) {
        LOG.debug("Adding route userName={}, lat={}, long={}, noOfPlaces={}, date={}, directionMode={}, address={}",
                userName, x, y, noOfPlaces, date, directionMode, address);
        Route route = new Route(userName, x, y, address, noOfPlaces, date, directionMode);
        routesDatabase.put(route.getRouteId(), route);
        return route;
    }

    public Collection<Route> getRoutes(DirectionMode directionMode, LocalDateTime startDate, LocalDateTime endDate) {
        LOG.debug("Getting routes directionMode={}, startDate={}, endDate={}", directionMode, startDate, endDate);
        return routesDatabase
                .values()
                .stream()
                .filter(route -> route.getDirectionMode() == directionMode)
                .filter(route -> isBetweenInclusive(startDate, endDate, route.getDate()))
                .collect(Collectors.toList());
    }

    private boolean isBetweenInclusive(LocalDateTime startDate, LocalDateTime endDate, LocalDateTime date) {
        if (date == null) {
            return true;
        } else if (startDate == null && endDate == null) {
            return true;
        } else if (startDate == null) {
            return date.isEqual(endDate) || date.isBefore(endDate);
        } else if (endDate == null) {
            return date.isEqual(startDate) || date.isAfter(startDate);
        } else {
            return date.isEqual(startDate)
                    || date.isEqual(endDate)
                    || (date.isAfter(startDate) && date.isBefore(endDate));
        }
    }

    public Route addPassenger(String routeId, String userName) {
        LOG.debug("Adding passenger routeId={}, userName={}", routeId, userName);
        Route route = routesDatabase.get(routeId);
        if (route == null) {
            throw new CarPoolingException("No such route " + routeId);
        } else {
            if (route.getUserName().equals(userName)) {
                throw new CarPoolingException(
                        "Driver (" + userName + ") cannot be added as a passenger to same route (" + routeId + ")");
            }
            Passenger passenger = new Passenger(userName);
            boolean success = route.getPassengers().add(passenger);
            if (!success) {
                LOG.warn("Passenger {} is already present in route {}", userName, routeId);
            }
        }
        return route;
    }

    @Override
    public void cancelRoute(String routeId) {
        LOG.debug("Cancelling route={}", routeId);
        Route removedRoute = routesDatabase.remove(routeId);
        if (removedRoute == null) {
            LOG.warn("Route {} was not present", routeId);
        }
    }

    @Override
    public Route removePassenger(String routeId, String userName) {
        LOG.debug("Removing passenger={} from route={}", userName, routeId);
        Route route = routesDatabase.get(routeId);
        Optional<Passenger> passengerToBeDeleted = route.getPassengers().stream().filter(passenger -> passenger.getPassengerName().equals(userName)).findFirst();
        if (passengerToBeDeleted.isPresent()) {
            boolean success = route.getPassengers().remove(passengerToBeDeleted.get());
            if (!success) {
                LOG.warn("Passenger {} already deleted from route {}", userName, routeId);
            }
        } else {
            LOG.warn("Passenger {} already deleted from route {}", userName, routeId);
        }
        return route;
    }
}
